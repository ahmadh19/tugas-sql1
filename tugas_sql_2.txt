1 Membuat Database
CREATE DATABASE myshop;

2 Membuat Table di Dalam Database

tabel users
CREATE TABLE users( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );

tabel categories
CREATE TABLE categories( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null );

tabel items
CREATE TABLE items( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, description varchar(255) NOT null, price int(8) NOT null, stock int(8) NOT null, category_id int(8) NOT null, FOREIGN KEY(category_id) REFERENCES categories(id) );


3 Membuat Data pada Table

tabel users
INSERT INTO users(name, email, password) VALUES("JOHN DOE","john@doe.com","john123"),("JANE DOE","jane@doe.com","jenita123");

tabel categories
INSERT INTO categories(name) VALUES("gadget"), ("cloth"),("men"),("women"),("branded");

tabel items
INSERT INTO items(name, description, price, stock, category_id) VALUES ("Samsung b50","hape keren dari merek samsung",4000000,100,1),("Uniklooh","baju keren dari brand ternama",500000,50,2),("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. Mengambil Data dari Database
a. Mengambil data users (kecuali variabel paswword)
SELECT id, name, email from users;
b. Mengambil data items (Kriteria: ambil data dengan harga > 1juta dan mengambil data dengan nama yang mirip)
SELECT * from items WHERE price > 1000000;
SELECT * FROM items WHERE description LIKE "%samsung";
c. Menampilkan data items join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name from items INNER JOIN categories on items.category_id = category_id;

5. Mengubah Database dari Database
UPDATE items set price=2500000 WHERE name = "samsung b50";


